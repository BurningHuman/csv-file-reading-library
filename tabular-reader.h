#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

//Arbitrary defined values, at least trying to introduce memory efficiency.
#define MAX_SIZE 4096
#define TOKEN_SIZE 1024

struct meta_data{
	char delimiter;
	unsigned long int columns;
	unsigned long int lines;
	bool header;
};

int compare_lines(struct meta_data *data_1, struct meta_data *data_2)
{
	return ((data_1->lines) != (data_2->lines)); 
}

//This method should check if everyline contains the same amount of columns in comparison to the first first line or if there are some inconsistencies with the indentation.
//
//It works basically like a delimiter counter, it checks the first line at first and stores the number of delimiters. Then it goes into a while loop where it iterates over the whole file and compares the counted delimiters with the delimiter from the first line. If they are unequal, the meta_data is broken.
int check_file(FILE *fp, char delimiter)
{
	unsigned long int lines = 0;
	unsigned long int columns = 0;
	unsigned int columns_compare = 0;
	char ch;
	

	//read in first line to set the column number every other line should have
	while((ch = fgetc(fp)) != '\n' && ch != EOF) //pay attention, EOF is also incounting as a line
	{
		columns += (ch == delimiter)*1;
	}
	lines++;
	if(ch == '\n')
	{
		while((ch = fgetc(fp)) != EOF)
		{
			columns_compare += (ch == delimiter)*1;
			if(ch == '\n')
			{
				lines++;
				if(columns != columns_compare){fprintf(stderr, "Error: In line %lu is an unequal number of columns in comparison to the first line.\n", lines); exit(-1);}
				columns_compare = 0;
			}
		}
		lines++;
	}
	fseek(fp, 0, SEEK_SET);
	if(lines == 1)
	{
		fprintf(stdout, "Dataset contains only one line!\n");
		return 1;
	}
	fprintf(stdout, "Dataset is fine. Checked %lu lines. It contains %lu columns\n", lines, columns+1);
	return 0;

}

//This function is specifically used for the init_meta_data() function.
int get_lincol(FILE *fp, char delimiter, struct meta_data *data)
{
	char ch;
	//First get the amount of columns
	while((ch = fgetc(fp)) != '\n')
	{
		if(ch == delimiter)
		{
			data->columns++;
		}
	}
	//Second get the amount of lines
	while((ch = fgetc(fp)) != EOF)
	{
		if(ch == '\n')
		{
			data->lines++;
		}
	}
	data->columns++; //there are always one more columns than delimiters in the file
	data->lines++; // the EOF counts in for the last line.
	return 0;
}

//Initiates meta data for a tabular file and writes it all into a struct, which is necessary for other functions
void init_meta_data(FILE *fp, char _delimiter, bool _header, struct meta_data *data)
{
	get_lincol(fp, _delimiter, data); 
	printf("columns: %lu\tlines: %lu\n", data->columns, data->lines);
	data->delimiter = _delimiter;
	data->header = _header;
}



char (*store_lines(FILE *fp, unsigned long int lines))[MAX_SIZE]
{
	//char **line_arr = malloc(lines*sizeof(char*));
	
	char (*line_arr)[MAX_SIZE] = (char(*)[MAX_SIZE]) malloc(lines*sizeof(char[MAX_SIZE]));
	for(unsigned long int i = 0; i < lines; i++)
	{
		//line_arr[i] = malloc(MAX_SIZE*sizeof(char));
		fgets(line_arr[i], MAX_SIZE, fp);
		//fprintf(stdout, "Line [%d]: %s\n", i, line_arr[i]);
	}
	fseek(fp, 0, SEEK_SET);
	return line_arr;
}

char **tokenize(FILE *fp, unsigned int wanted_column, struct meta_data *data)
{			
	char (*strings)[MAX_SIZE] = store_lines(fp, data->lines); //Getting the lines for processing
	char token_buffer[TOKEN_SIZE];
	char **tokens = malloc(data->lines*sizeof(char*));
	unsigned int actual_column = 0;
	
	size_t line_len;
	unsigned int b = 0;
	unsigned int offset;
	

	//If a columns is wanted that is not the 0th column
	if(wanted_column > 0)
	{
		//Iterating over every line
		for(unsigned long int t = 0; t < data->lines; t++)
		{
			line_len = strlen(strings[t]);
			actual_column = 0;
			
			//Iterates over every character in one line
			for(unsigned int i = 0; i < line_len; i++)
			{
				if(strings[t][i] == data->delimiter)
				{
					actual_column++;
					//printf("actual column: %d\n", actual_column);
					if(actual_column == wanted_column)
					{
						offset=i; //offset of the token from the beginning of the line
						i++; //skipping the first \t
						b = 0;
						for(; i < line_len; i++)
						{
							if(strings[t][i] == data->delimiter || strings[t][i] == '\n'){break;}
							//printf("b: %d\n", b);
							//printf("i: %d\n", i);
							token_buffer[b] = strings[t][i];
							b++;

						}
						token_buffer[b++] = '\0';
						tokens[t] = malloc((i-offset)*sizeof(char));
						//fprintf(stdout, "token buffer: %s\n", token_buffer);
						strcpy(tokens[t], token_buffer);
						//fprintf(stdout, "token buffer heap: %s\n", tokens[t]);
						//free(strings[t]);	
						break;
					}

				}
			}
		}
	}
	else
	{
		unsigned int b;
		int i = 0;
		for(unsigned long int t = 0; t < data->lines; t++)
		{
			line_len = strlen(strings[t]);
			b = 0;
			i = 0;
			for(; i < line_len; i++)
			{
				if(strings[t][i] == data->delimiter || strings[t][i] == '\n'){break;}
				//printf("b: %d\n", b);
				//printf("i: %d\n", i);
				token_buffer[b] = strings[t][i];
				b++;
			}
			token_buffer[b++] = '\0'; // completes the copied string.
			tokens[t] = malloc(i*sizeof(char));
			//fprintf(stdout, "token buffer: %s\n", token_buffer);
			strcpy(tokens[t], token_buffer);
			//fprintf(stdout, "token buffer heap: %s\n", tokens[t]);
		}

	}
	free(strings);
	return tokens;

}

int *tokenize_atoi(FILE *fp, unsigned int wanted_column, struct meta_data *data)
{			
	char (*strings)[MAX_SIZE] = store_lines(fp, data->lines);
	char token_buffer[TOKEN_SIZE];
	char **tokens_str = malloc(data->lines*sizeof(char*));
	int *tokens_num = malloc(data->lines*sizeof(int));
	unsigned int actual_column = 0;
	
	size_t line_len;
	int b = 0;
	unsigned int offset;
	unsigned int current_line = 0;

	if(data->header == true)
	{
		if(wanted_column > 0)
		{
			for(current_line = 1; current_line < data->lines; current_line++)
			{
				//fprintf(stdout, "\nt: %d\n\n", t);
				line_len = strlen(strings[current_line]);
				actual_column = 0;
				for(int i = 0; i < line_len; i++)
				{
					if(strings[current_line][i] == data->delimiter)
					{
						actual_column++;
						//printf("actual column: %d\n", actual_column);
						if(actual_column == wanted_column)
						{
							offset=i; //offset of the token from the beginning of the line
							i++; //skipping the first \t
							b = 0;
							for(; i < line_len; i++)
							{
								if(strings[current_line][i] == data->delimiter || strings[current_line][i] == '\n'){break;}
								//printf("b: %d\n", b);
								//printf("i: %d\n", i);
								token_buffer[b] = strings[current_line][i];
								b++;
	
							}
							token_buffer[b++] = '\0';
							tokens_str[current_line] = malloc((i-offset)*sizeof(char));
							//fprintf(stdout, "token buffer: %s\n", token_buffer);
							strcpy(tokens_str[current_line], token_buffer);
							tokens_num[current_line] = atoi(tokens_str[current_line]);
							//fprintf(stdout, "token buffer heap: %s\n", tokens[current_line]);
							break;
						}
	
					}
				}
			}
		}
		else
		{
			unsigned int b;
			int i = 0;
			for(int t = 0; t < data->lines; t++)
			{
				line_len = strlen(strings[current_line]);
				b = 0;
				i = 0;
				for(; i < line_len; i++)
				{
					if(strings[current_line][i] == data->delimiter || strings[current_line][i] == '\n'){break;}
					//printf("b: %d\n", b);
					//printf("i: %d\n", i);
					token_buffer[b] = strings[current_line][i];
					b++;
				}
				token_buffer[b++] = '\0'; // completes the copied string.
				tokens_str[current_line] = malloc(i*sizeof(char));
				//fprintf(stdout, "token buffer: %s\n", token_buffer);
				strcpy(tokens_str[current_line], token_buffer);
				//fprintf(stdout, "token buffer heap: %s\n", tokens[current_line]);
				tokens_num[current_line] = atoi(tokens_str[current_line]);
			}
	
		}
	}
	else
	{
		if(wanted_column > 0)
		{
			for(current_line = 0; current_line < data->lines; current_line++)
			{
				//fprintf(stdout, "\nt: %d\n\n", t);
				line_len = strlen(strings[current_line]);
				actual_column = 0;
				for(int i = 0; i < line_len; i++)
				{
					if(strings[current_line][i] == data->delimiter)
					{
						actual_column++;
						//printf("actual column: %d\n", actual_column);
						if(actual_column == wanted_column)
						{
							offset=i; //offset of the token from the beginning of the line
							i++; //skipping the first \t
							b = 0;
							for(; i < line_len; i++)
							{
								if(strings[current_line][i] == data->delimiter || strings[current_line][i] == '\n'){break;}
								//printf("b: %d\n", b);
								//printf("i: %d\n", i);
								token_buffer[b] = strings[current_line][i];
								b++;
	
							}
							token_buffer[b++] = '\0';
							tokens_str[current_line] = malloc((i-offset)*sizeof(char));
							//fprintf(stdout, "token buffer: %s\n", token_buffer);
							strcpy(tokens_str[current_line], token_buffer);
							tokens_num[current_line] = atoi(tokens_str[current_line]);
							//fprintf(stdout, "token buffer heap: %s\n", tokens[current_line]);
							break;
						}
	
					}
				}
			}
		}
		else
		{
			unsigned int b;
			int i = 0;
			for(int t = 0; t < data->lines; t++)
			{
				line_len = strlen(strings[current_line]);
				b = 0;
				i = 0;
				for(; i < line_len; i++)
				{
					if(strings[current_line][i] == data->delimiter || strings[current_line][i] == '\n'){break;}
					//printf("b: %d\n", b);
					//printf("i: %d\n", i);
					token_buffer[b] = strings[current_line][i];
					b++;
				}
				token_buffer[b++] = '\0'; // completes the copied string.
				tokens_str[current_line] = malloc(i*sizeof(char));
				//fprintf(stdout, "token buffer: %s\n", token_buffer);
				strcpy(tokens_str[current_line], token_buffer);
				//fprintf(stdout, "token buffer heap: %s\n", tokens[current_line]);
				tokens_num[current_line] = atoi(tokens_str[current_line]);
			}
	
		}
	}
	
	free(strings);
	free(tokens_str);
	return tokens_num;
}

float *tokenize_atof(FILE *fp, unsigned int wanted_column, struct meta_data *data)
{			
	char (*strings)[MAX_SIZE] = store_lines(fp, data->lines);
	char token_buffer[TOKEN_SIZE];
	char **tokens_str = malloc(data->lines*sizeof(char*));
	float *tokens_num = malloc(data->lines*sizeof(float));
	unsigned int actual_column = 0;
	
	size_t line_len;
	int b = 0;
	unsigned int offset;
	unsigned int current_line = 0;
	if(data->header == true)
	{
		if(wanted_column > 0)
		{
			for(current_line = 1; current_line < data->lines; current_line++)
			{
				//fprintf(stdout, "\nt: %d\n\n", t);
				line_len = strlen(strings[current_line]);
				actual_column = 0;
				for(int i = 0; i < line_len; i++)
				{
					if(strings[current_line][i] == data->delimiter)
					{
						actual_column++;
						//printf("actual column: %d\n", actual_column);
						if(actual_column == wanted_column)
						{
							offset=i; //offset of the token from the beginning of the line
							i++; //skipping the first \t
							b = 0;
							for(; i < line_len; i++)
							{
								if(strings[current_line][i] == data->delimiter || strings[current_line][i] == '\n'){break;}
								//printf("b: %d\n", b);
								//printf("i: %d\n", i);
								token_buffer[b] = strings[current_line][i];
								b++;
	
							}
							token_buffer[b++] = '\0';
							tokens_str[current_line] = malloc((i-offset)*sizeof(char));
							//fprintf(stdout, "token buffer: %s\n", token_buffer);
							strcpy(tokens_str[current_line], token_buffer);
							tokens_num[current_line] = atof(tokens_str[current_line]); 
							//fprintf(stdout, "token buffer heap: %s\n", tokens[current_line]);
							break;
						}
	
					}
				}
			}
		}
		else
		{
			unsigned int b;
			int i = 0;
			for(current_line = 1; current_line < data->lines; current_line++)
			{
				line_len = strlen(strings[current_line]);
				b = 0;
				i = 0;
				for(; i < line_len; i++)
				{
					if(strings[current_line][i] == data->delimiter || strings[current_line][i] == '\n'){break;}
					//printf("b: %d\n", b);
					//printf("i: %d\n", i);
					token_buffer[b] = strings[current_line][i];
					b++;
				}
				token_buffer[b++] = '\0'; // completes the copied string.
				tokens_str[current_line] = malloc(i*sizeof(char));
				//fprintf(stdout, "token buffer: %s\n", token_buffer);
				strcpy(tokens_str[current_line], token_buffer);
				tokens_num[current_line] = atof(tokens_str[current_line]); 
				//fprintf(stdout, "token buffer heap: %s\n", tokens[current_line]);
			}
	
		}
	}
	else
	{
		if(wanted_column > 0)
		{
			for(current_line = 0; current_line < data->lines; current_line++)
			{
				//fprintf(stdout, "\nt: %d\n\n", t);
				line_len = strlen(strings[current_line]);
				actual_column = 0;
				for(int i = 0; i < line_len; i++)
				{
					if(strings[current_line][i] == data->delimiter)
					{
						actual_column++;
						//printf("actual column: %d\n", actual_column);
						if(actual_column == wanted_column)
						{
							offset=i; //offset of the token from the beginning of the line
							i++; //skipping the first \t
							b = 0;
							for(; i < line_len; i++)
							{
								if(strings[current_line][i] == data->delimiter || strings[current_line][i] == '\n'){break;}
								//printf("b: %d\n", b);
								//printf("i: %d\n", i);
								token_buffer[b] = strings[current_line][i];
								b++;
	
							}
							token_buffer[b++] = '\0';
							tokens_str[current_line] = malloc((i-offset)*sizeof(char));
							//fprintf(stdout, "token buffer: %s\n", token_buffer);
							strcpy(tokens_str[current_line], token_buffer);
							tokens_num[current_line] = atof(tokens_str[current_line]); 
							//fprintf(stdout, "token buffer heap: %s\n", tokens[current_line]);
							break;
						}
	
					}
				}
			}
		}
		else
		{
			unsigned int b;
			int i = 0;
			for(current_line = 0; current_line < data->lines; current_line++)
			{
				line_len = strlen(strings[current_line]);
				b = 0;
				i = 0;
				for(; i < line_len; i++)
				{
					if(strings[current_line][i] == data->delimiter || strings[current_line][i] == '\n'){break;}
					//printf("b: %d\n", b);
					//printf("i: %d\n", i);
					token_buffer[b] = strings[current_line][i];
					b++;
				}
				token_buffer[b++] = '\0'; // completes the copied string.
				tokens_str[current_line] = malloc(i*sizeof(char));
				//fprintf(stdout, "token buffer: %s\n", token_buffer);
				strcpy(tokens_str[current_line], token_buffer);
				tokens_num[current_line] = atof(tokens_str[current_line]); 
				//fprintf(stdout, "token buffer heap: %s\n", tokens[current_line]);
			}
	
		}
	}
	free(strings);
	free(tokens_str);
	return tokens_num;

}

int *atoi_arr(char **strings, unsigned int lines)
{
	int *numbers = malloc(lines*sizeof(int));
	for(int i = 0; i < lines; i++)
	{
		numbers[i] = atoi(strings[i]);	
		free(strings[i]);
	}
	free(strings);

	return numbers;
}
float *atof_arr(char **strings, unsigned int lines)
{
	float *numbers = malloc(lines*sizeof(float));
	for(int i = 0; i < lines; i++)
	{
		numbers[i] = atof(strings[i]);
		free(strings[i]);
	}
	free(strings);
	return numbers;
}

int find_multiples_int(int *numbers, int compare, unsigned int lines)
{
	int multiples = 0;
	for(int i = 0; i < lines; i++)
	{
		if(numbers[i] == compare)
		{
			multiples++;
		}
	}
	return multiples;

}

int find_multiples_float(float *numbers, float compare, unsigned int lines)
{
	int multiples = 0;
	for(int i = 0; i < lines; i++)
	{
		if(numbers[i] == compare)
		{
			multiples++;
		}
	}
	return multiples;
}

int find_multiples_string(char **strings, char *compare, unsigned int lines)
{
	int multiples = 0;
	for(int i = 0; i < lines; i++)
	{	
		if(strcmp(strings[i], compare) == 0)
		{
			multiples++;
		}
	}
	return multiples;
}

