#include <stdio.h>
#include <stdlib.h>
#include "tabular-reader.h"

int main()
{
	FILE *fp;
	char *path = "weather_data.csv";
	if((fp = fopen(path, "r")) == NULL){fprintf(stderr,"Could not open file %s!", path);exit(-1);}
	check_file(fp, ',');
	return 0;
}
